﻿#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "Animal sound" << std::endl;
    }
};

class Cat : public Animal
{
public:
	Cat() {}
    void Voice() override
    {
        std::cout << "Cat says: Me-e-e-e-o-o-o-w-w" << std::endl;
    }
};

class Dog : public Animal
{
public:
	Dog() {}
    void Voice() override
    {
        std::cout << "Dog says: Woof-woof!" << std::endl;
    }
};

class Elephant : public Animal
{
public:
    Elephant() {}
    void Voice() override
    {
        std::cout << "Wait.... What does the elephant says?!" << std::endl;
    }
};

int main()
{
    Animal* zoo[3] = { new Dog(), new Cat(), new Elephant() };

    for (int i = 0; i < 3; i++) 
    {
        zoo[i]->Voice();
    }
}